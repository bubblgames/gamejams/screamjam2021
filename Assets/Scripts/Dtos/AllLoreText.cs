using System.Collections.Generic;

namespace Dtos
{
    public static class AllLoreText
    {
        
        public static List<string> GetOpeningLoreText()
        {
            return new List<string>
            {
                "You've applied for your dream job, a game developer, and you've been hired! However, you were so excited that you partied all night and as you were going to sleep you remembered there was a Terms & Conditions you agreed to. And you felt a sinking feeling in your stomach, what did I agree to?!",
                "You rush to your PC to check it out and as you read it says \"... must immediately assume responsibilities of the Happy Head Salesman as soon as possible...\" Your heart sinks.. and you hear a knock at the door.",
                "You meet a man with a terrible grin, who pulls you out of your house onto the street, throws a bag of heads at you, and tells you the following information.",
                "\"You've been met with a terrible fate, you must feed the hungry zombies. If you want more press 'C' and I'll ship some to you. Move with WASD, aim with the mouse and click to throw a head to a zombie. Zombies only eat heads the same color as their body, but will change body color to the last head they ate.\"",
                "WASD? 'C', Mouse?! what was this strange man talking about?! Before I could ask him, he threw me a backpack of heads, and I started hearing, seeing, and smelling things I've never experienced before..."
            };
        }
        
        public static List<string> GetIntermissionLoreText()
        {
            return new List<string>
            {
                "This has gone on long enough, I can't keep doing this! I need to think of a plan. Unfortunately the only plan I can think of is to find some sucker like me to deal with this terrifying job.",
                "I race through more zombies, climb the side of my house, up to my bedroom window. I go in, start my computer, and repost the same job posting that was sent for me.",
                "My heart is racing, and luckily within seconds, someone applies!",
                "I immediately accept them and see their address, not too far away! I start to go outside and notice something is changed, no zombies to be seen!!",
                "I race down the street as fast as I can, the address is just about in sight when I feel the ground shake...",
                "I turn around to see the most incredibly horrific thing in my life!!",
                "As I turn to run, I notice it's already surrounded me, I look to my full backpack and do the only thing that makes sense to me... FEED!"
            };
        }
        
        public static List<string> GetClosingLoreText()
        {
            return new List<string>
            {
                "I made it to his house!!!!!",
                "I knock on the door....",
                "He answers, with the empty pale face that I can relate to, but I can't help to grin and know that my terrible fate has come to an end. All I can say to him is...",
                "\"You've been met with a terrible fate...\""
            };
        }
    }
}