using System.Collections.Generic;

namespace Dtos
{
    public static class AllLevelData
    {
        public static List<LevelData> GetLevelData()
        {
            return new List<LevelData>
            {
                new LevelData
                {
                    Zombies = 10,
                    MaxSpeed = 5,
                    MinSpeed = 5,
                    MaxSpawnRate = 2f,
                    MinSpawnRate = 2f
                },
                new LevelData
                {
                    Zombies = 10,
                    MaxSpeed = 60,
                    MinSpeed = 20,
                    MaxSpawnRate = 2f,
                    MinSpawnRate = 0.2f
                },
                new LevelData
                {
                    Zombies = 10,
                    MaxSpeed = 80,
                    MinSpeed = 30,
                    MaxSpawnRate = 2f,
                    MinSpawnRate = 0.2f
                },
                new LevelData
                {
                    Zombies = 10,
                    MaxSpeed = 30,
                    MinSpeed = 30,
                    MaxSpawnRate = 2f,
                    MinSpawnRate = 0.2f
                },
                new LevelData
                {
                    Zombies = 3,
                    MaxSpeed = 30,
                    MinSpeed = 30,
                    MaxSpawnRate = 2f,
                    MinSpawnRate = 0.2f
                },
            };
        }
    }
}