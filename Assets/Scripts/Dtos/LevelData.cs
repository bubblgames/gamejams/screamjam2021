namespace Dtos
{
    public class LevelData
    {
        public int Zombies { get; set; }
        public float MinSpeed { get; set; }
        public float MaxSpeed { get; set; }
        public float MinSpawnRate { get; set; }
        public float MaxSpawnRate { get; set; }
    }
}