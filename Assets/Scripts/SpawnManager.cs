using System.Collections.Generic;
using Dtos;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private GameObject zombiePrefab;
    [SerializeField] private float boundary = 150f;
    private List<LevelData> _levelData;
    private LevelData _currentLevelData;
    private GameManager gameManager;
    private List<Color> colors = new List<Color>
    {
        Color.black,
        Color.yellow,
        Color.magenta,
    };
    
    public int killedZombies = 0;
    public int createdZombies = 0;
    public bool isSpawning = false;

    public void Awake()
    {
        _levelData = AllLevelData.GetLevelData();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    
    public void SetLevelData(int currentLevel)
    {
        killedZombies = 0;
        createdZombies = 0;
        _currentLevelData = _levelData[currentLevel - 1];
        gameManager.UpdateWaveNumber(currentLevel);
        gameManager.UpdateZombieCount(_currentLevelData.Zombies);
    }

    public int GetTotalLevels()
    {
        return _levelData.Count;
    }

    public bool LevelHasBeenCompleted()
    {
        return _currentLevelData.Zombies == killedZombies;
    }
    
    public int GetTotalZombieCount()
    {
        return _currentLevelData.Zombies;
    }

    public void ZombieDied()
    {
        killedZombies++;
        gameManager.UpdateZombieCount(_currentLevelData.Zombies - killedZombies);
    }

    public void InitializeSpawning()
    {
        isSpawning = true;
        Invoke("SpawnZombie", 1f);
    }
    
    public void SpawnZombie()
    {
        var zombie = Instantiate(zombiePrefab, GenerateSpawnPosition(zombiePrefab, boundary), zombiePrefab.transform.rotation);
        zombie.GetComponent<FollowPlayer>().speed = Random.Range(_currentLevelData.MinSpeed, _currentLevelData.MaxSpeed);
        zombie.GetComponent<Renderer>().material.color = colors[Random.Range(0,3)];
        createdZombies++;
        if (createdZombies >= _currentLevelData.Zombies)
        {
            isSpawning = false;
        }
        else
        {
            Invoke("SpawnZombie", Random.Range(_currentLevelData.MinSpawnRate, _currentLevelData.MaxSpawnRate));
        }
    }

    private Vector3 GenerateSpawnPosition(GameObject prefab, float boundary)
    {
        var randomIndex = Random.Range(0, 4);
        switch (randomIndex)
        {
            case 0:
                return new Vector3(-boundary, 0,
                    Random.Range(-boundary, boundary));
            case 1:
                return new Vector3(boundary, 0,
                    Random.Range(-boundary, boundary));
            case 2:
                return new Vector3(Random.Range(-boundary, boundary), 0,
                    -boundary);
            case 3:
                return new Vector3(Random.Range(-boundary, boundary), 0,
                    boundary);
        }
        
        return new Vector3(-boundary, prefab.transform.localScale.y,
            Random.Range(-boundary, boundary)); 
    }
}
