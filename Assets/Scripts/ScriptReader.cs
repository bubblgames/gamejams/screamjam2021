using System.Collections;
using System.Collections.Generic;
using Dtos;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptReader : MonoBehaviour
{
    
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private TextMeshProUGUI continueText;
    [SerializeField] int nextSceneIndex;
    
    private int currentIndex = 0;
    private int currentLetter = 0;
    private List<string> textToPrint;
    private float letterFrequency = 0.05f;
    private float fastLetterFrequency = 0.001f;
    private float normalLetterFrequency = 0.05f;
    private float blinkTime = 1f;
    private bool blinkIsActive = false;
    private bool isBlinking = false;

    void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            textToPrint = AllLoreText.GetOpeningLoreText();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            textToPrint = AllLoreText.GetIntermissionLoreText();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            textToPrint = AllLoreText.GetClosingLoreText();
        }
        
    }

    public void Start()
    {
        StartPlayingText();
    }

    public void StartPlayingText()
    {
        currentLetter = 0;
        blinkIsActive = false;
        isBlinking = false;
        text.text = "";
        Invoke("ShowText", letterFrequency);
    }

    public void ShowText()
    {
        text.text += textToPrint[currentIndex][currentLetter];
        currentLetter++;
        if (currentLetter == textToPrint[currentIndex].Length)
        {
            InitializeBlinking();
        }
        else
        {
            Invoke("ShowText", letterFrequency);
        }
    }

    public void InitializeBlinking()
    {
        InvokeRepeating("Blink",0f, blinkTime);
        isBlinking = true;
    }

    public void StopBlinking()
    {
        isBlinking = false;
        continueText.gameObject.SetActive(false);
        CancelInvoke("Blink");
    }
    
    public void Blink()
    {
        blinkIsActive = !blinkIsActive;
        continueText.gameObject.SetActive(blinkIsActive);
    }

    void Update()
    {
        
        if (Input.anyKey)
        {
            if (isBlinking)
            {
                StopBlinking();
                currentIndex++;
                if (currentIndex == textToPrint.Count)
                {
                    SceneManager.LoadScene(nextSceneIndex);
                }
                else
                {
                    letterFrequency = normalLetterFrequency;
                    StartPlayingText();
                }
            }
            else
            {
                letterFrequency = fastLetterFrequency;
            }
        } 
    }
}
