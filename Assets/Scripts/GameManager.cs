using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private SpawnManager _spawnManager;
    [SerializeField] private PlayerUIManager _playerUIManager;
    
    private int currentLevel;
    
    public void Start()
    {
        currentLevel = 0;
        StartNextLevel();
    }

    private void StartNextLevel()
    {
        currentLevel++;
        _spawnManager.SetLevelData(currentLevel);
        //isPlaying = true;
        //_spawnManager.InitializeSpawning();
        StartCoroutine(_uiManager.ShowIntroWave(currentLevel, _spawnManager.GetTotalZombieCount()));
    }
    
    public void IntroUIFinished()
    {
        _playerUIManager.isPlaying = true;
        _spawnManager.InitializeSpawning();
    }

    public void Update()
    {
        if (_playerUIManager.isPlaying && _spawnManager.LevelHasBeenCompleted())
        {
            LevelEnd();
        }
    }
    private void LevelEnd()
    {
        _playerUIManager.isPlaying = false;
        StartCoroutine(_uiManager.ShowEndWave());
    }

    public void EndUIFinished()
    {
        if (currentLevel == _spawnManager.GetTotalLevels())
        {
            SceneManager.LoadScene(3);
        }
        else
        {
            StartNextLevel();
        }
    }

    public void UpdateWaveNumber(int zombieCount)
    {
        _uiManager.UpdateWaveNumber(zombieCount);
    }

    public void UpdateZombieCount(int zombieCount)
    {
        _uiManager.UpdateZombieCount(zombieCount);
    }
}
