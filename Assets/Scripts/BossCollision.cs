using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCollision : MonoBehaviour
{
    [SerializeField] private int health = 2;
    private BossManager _bossManager;
    private List<Color> colors = new List<Color>
    {
        Color.black,
        Color.yellow,
        Color.magenta
    };

    [SerializeField] private float colorChangeMin = 1f;
    [SerializeField] private float colorChangeMax = 4f;
    [SerializeField] private float minSpeed = 10f;
    [SerializeField] private float maxSpeed = 90f;
    
    public void Awake()
    {
        _bossManager = GameObject.Find("BossManager").GetComponent<BossManager>();
    }
    void Start()
    {
        _bossManager.UpdateHealth(health);
        Invoke("ChangeColor",0f);
    }

    private void ChangeColor()
    {
        GetComponent<Renderer>().material.color = colors[Random.Range(0, colors.Count)];
        Invoke("ChangeColor", Random.Range(colorChangeMin, colorChangeMax));
        GetComponent<FollowPlayer>().speed = Random.Range(minSpeed, maxSpeed);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Head"))
        {
             if (GetComponent<Renderer>().material.color.Equals(other.gameObject.GetComponent<Renderer>().material.color))
             {
                if (health == 1)
                {
                    Destroy(gameObject);
                    _bossManager.BossDied();
                }
                else
                {
                    health--;
                    if (GetComponent<FollowPlayer>().speed >= 20f)
                    {
                        GetComponent<FollowPlayer>().speed -= 10f;
                    }
                    _bossManager.UpdateHealth(health);
                }
             }
             Destroy(other.gameObject);
        }
        else if (other.gameObject.tag.Equals("Player"))
        {
            
        }
    }
}
