using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    
    [SerializeField] private float boundary = 300f;
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        if (BoundaryCheck.IsOutOfBounds(transform.position.x, boundary) || 
            BoundaryCheck.IsOutOfBounds(transform.position.z, boundary))
        {
            Destroy(gameObject);
        }
    }
}
