using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ThrowingManager : MonoBehaviour
{
    private Material currentMaterial;

    private List<Color> colors = new List<Color>
    {
        Color.black,
        Color.yellow,
        Color.magenta
    };

    private Dictionary<Color, int> ammo = new Dictionary<Color, int>
    {
        {Color.black, 10},
        {Color.yellow, 10},
        {Color.magenta, 10},
    };

    private int selectedColorIndex = 0;
    
    [SerializeField] private GameObject headPrefab;
    
    [SerializeField] private PlayerUIManager playerUIManager;

    
    // Start is called before the first frame update
    void Start()
    {
        UpdateAmmoCounts();
    }

    void UpdateAmmoCounts()
    {
        for (var i = 0; i < colors.Count; i++)
        {
            playerUIManager.UpdateAmmoText(i, ammo[colors[i]]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerUIManager.isPlaying)
        {
            ThrowHead();
        }
        
        ChangeHeadColor();
    }
    
    void ThrowHead()
    {
        if (Input.GetMouseButtonDown(0) && ammo[colors[selectedColorIndex]] > 0)
        {
            var head = Instantiate(headPrefab, transform.position, transform.rotation);
            head.GetComponent<Renderer>().material.color = colors[selectedColorIndex];
            ammo[colors[selectedColorIndex]]--;
            playerUIManager.UpdateAmmoText(selectedColorIndex, ammo[colors[selectedColorIndex]]);
        }
    }  
    
    void ChangeHeadColor()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            selectedColorIndex = 0;
            playerUIManager.UpdateHeadColor(selectedColorIndex);
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            selectedColorIndex = 1;
            playerUIManager.UpdateHeadColor(selectedColorIndex);
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            selectedColorIndex = 2;
            playerUIManager.UpdateHeadColor(selectedColorIndex);
        }
    }

    public void Reload(Color packageColor, int number)
    {
        ammo[packageColor] += number;
        playerUIManager.UpdateAmmoText(colors.IndexOf(packageColor), ammo[packageColor]);
    }
}
