using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovementManager : MonoBehaviour
{
    [SerializeField] private float speed = 80f;
    public float boundary = 150f;
    [SerializeField] private PlayerUIManager playerUIManager;

    void Update()
    {
        if (playerUIManager.isPlaying)
        {
            Move();
        }
        Look();
    }

    void Move()
    {
        var transformObject = transform;
        if (!BindMovement(transformObject))
        {
            float horizontalMovement = Input.GetAxis("Horizontal");
            float verticalMovement = Input.GetAxis("Vertical");
            transformObject.Translate(Vector3.forward * Time.deltaTime * verticalMovement * speed,Space.World);
            transformObject.Translate(Vector3.right * Time.deltaTime * horizontalMovement * speed,Space.World);
        }
    }

    bool BindMovement(Transform transformObject)
    {
        var transformPosition = transformObject.position;
        if (BoundaryCheck.IsOutOfBounds(transformPosition.x, boundary)){
            transform.position = new Vector3(transformPosition.x > 0 ? boundary : -boundary, transformPosition.y,
                transformPosition.z);
            return true;
        }
        if (BoundaryCheck.IsOutOfBounds(transformPosition.z, boundary)){
            transform.position = new Vector3(transformPosition.x, transformPosition.y,
                transformPosition.z > 0 ? boundary : -boundary);
            return true;
        }

        return false;
    }

    void Look()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out hit)) {
            transform.LookAt(hit.point);
            var rot = transform.eulerAngles;
            rot.x = 0;
            transform.eulerAngles = rot;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Zombie"))
        {
            SceneManager.LoadScene(2);
        }
    }
}
