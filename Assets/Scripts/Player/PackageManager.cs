using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackageManager : MonoBehaviour
{
    private Material currentMaterial;
    private List<Color> colors = new List<Color>
    {
        Color.black,
        Color.yellow,
        Color.magenta
    };
    private int selectedColorIndex = 0;
    [SerializeField] private GameObject packagePrefab;
    
    [SerializeField] private float boundary = 150f;
    [SerializeField] private float packageFrequencyTime = 10f;
    private float timeUntilCanRequestPackage = 0f;
    [SerializeField] private float packageArrivalTime = 5f;
    private float timeUntilPackageArrives = 0f;
    [SerializeField] private PlayerUIManager playerUIManager;

    void Update()
    {
        if (playerUIManager.isPlaying)
        {
            CheckInput();
        }
        
        ChangePackageColor();

        UpdateTimers();
    }

    void UpdateTimers()
    {
        timeUntilCanRequestPackage = CountdownOrStabilize(timeUntilCanRequestPackage);
        timeUntilPackageArrives = CountdownOrStabilize(timeUntilPackageArrives);

        UpdateTimersUI();
    }

    float CountdownOrStabilize(float timer)
    {
        return timer <= 0 ? 0f : timer - Time.deltaTime;
    }

    void UpdateTimersUI()
    {
        playerUIManager.UpdateTimersUI(timeUntilCanRequestPackage, timeUntilPackageArrives);
    }

    void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.C) && timeUntilCanRequestPackage <= 0)
        {
            CallForPackage();
            timeUntilCanRequestPackage = packageFrequencyTime;
            timeUntilPackageArrives = packageArrivalTime;
        }
    }

    void CallForPackage()
    {
        StartCoroutine(SpawnPackage());
    }

    IEnumerator SpawnPackage()
    {
        yield return new WaitForSeconds(packageArrivalTime);
        var package = Instantiate(packagePrefab, GenerateSpawnPosition(), packagePrefab.transform.rotation);
        package.GetComponent<Renderer>().material.color = colors[selectedColorIndex];
    }

    private Vector3 GenerateSpawnPosition()
    {
        return new Vector3(Random.Range(-boundary, boundary), 1,
            Random.Range(-boundary, boundary));
    }

    void ChangePackageColor()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            selectedColorIndex = 0;
            playerUIManager.UpdatePackageColor(selectedColorIndex);
        }
        
        if (Input.GetKeyDown(KeyCode.K))
        {
            selectedColorIndex = 1;
            playerUIManager.UpdatePackageColor(selectedColorIndex);
        }
        
        if (Input.GetKeyDown(KeyCode.L))
        {
            selectedColorIndex = 2;
            playerUIManager.UpdatePackageColor(selectedColorIndex);
        }
    }
}
