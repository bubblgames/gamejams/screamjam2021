using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackageController : MonoBehaviour
{
    private ThrowingManager throwingManager;

    void Awake()
    {
        throwingManager = GameObject.Find("Player").GetComponent<ThrowingManager>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            var packageColor = gameObject.GetComponent<Renderer>().material.color;
            if (packageColor.Equals(Color.white))
            {
                
            }
            else
            {
                throwingManager.Reload(packageColor, 3);
            }
            
            Destroy(gameObject);
        }
    }
}
