using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIManager : MonoBehaviour
{
    private UIColorManager _headColor;
    private UIColorManager _packageColor;
    private AmmoUIManager _ammo;
    private TimersUIManager _timers;
    public bool isPlaying = false;
    void Awake()
    {
        _headColor = GetComponents<UIColorManager>()[0];
        _packageColor = GetComponents<UIColorManager>()[1];
        _ammo = GetComponent<AmmoUIManager>();
        _timers = GetComponent<TimersUIManager>();
    }

    public void UpdateHeadColor(int selectedIndex)
    {
        _headColor.UpdateColor(selectedIndex);
    }
    
    public void UpdatePackageColor(int selectedIndex)
    {
        _packageColor.UpdateColor(selectedIndex);
    }
    
    public void UpdateAmmoText(int colorIndex, int ammo)
    {
        _ammo.UpdateAmmoText(colorIndex, ammo);
    }
    
    public void UpdateTimersUI(float timeUntilCanRequestPackage, float timeUntilPackageArrives)
    {
        _timers.UpdateTimersUI(timeUntilCanRequestPackage, timeUntilPackageArrives);
    }
}
