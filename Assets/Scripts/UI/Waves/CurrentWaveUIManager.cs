using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CurrentWaveUIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI waveNumberText;
    [SerializeField] private TextMeshProUGUI zombieText;
    
    public void UpdateWaveNumber(int waveNumber)
    {
        waveNumberText.text = "Wave: " + waveNumber;
    }
    
    public void UpdateZombieCount(int zombieCount)
    {
        zombieText.text = "Zombies Left: " + zombieCount;
    }
}
