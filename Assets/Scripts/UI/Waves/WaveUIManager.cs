using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveUIManager : MonoBehaviour
{
    [SerializeField] private GameObject waveIntroText;
    [SerializeField] private TextMeshProUGUI waveNumberText;
    [SerializeField] private TextMeshProUGUI zombieText;
    [SerializeField] private GameObject sellText;
    [SerializeField] private GameObject waveEndText;

    private GameManager gameManager;

    void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    public IEnumerator ShowIntroWave(int currentLevel, int numberOfZombies)
    {
        waveIntroText.SetActive(true);
        waveNumberText.text = "Wave: " + currentLevel;
        zombieText.text = "Zombies: " + numberOfZombies;
        yield return new WaitForSeconds(3f);
        waveIntroText.SetActive(false);
        StartCoroutine(ShowSellText());
    }
    
    public IEnumerator ShowSellText()
    {
        sellText.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        sellText.SetActive(false);
        gameManager.IntroUIFinished();
    }
    
    public IEnumerator ShowEndWave()
    {
        yield return new WaitForSeconds(1f);
        waveEndText.SetActive(true);
        yield return new WaitForSeconds(3f);
        waveEndText.SetActive(false);
        gameManager.EndUIFinished();
    }
}
