using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private WaveUIManager _wave;
    private UIColorManager _headColor;
    private UIColorManager _packageColor;
    private AmmoUIManager _ammo;
    private TimersUIManager _timers;
    private CurrentWaveUIManager _currentWave;

    void Awake()
    {
        _wave = GetComponent<WaveUIManager>();
        _headColor = GetComponents<UIColorManager>()[0];
        _packageColor = GetComponents<UIColorManager>()[1];
        _ammo = GetComponent<AmmoUIManager>();
        _timers = GetComponent<TimersUIManager>();
        _currentWave = GetComponent<CurrentWaveUIManager>();
    }

    public IEnumerator ShowIntroWave(int currentLevel, int numberOfZombies)
    {
        return _wave.ShowIntroWave(currentLevel, numberOfZombies);
    }
    
    public IEnumerator ShowEndWave()
    {
        return _wave.ShowEndWave();
    }
    
    public void UpdateHeadColor(int selectedIndex)
    {
        _headColor.UpdateColor(selectedIndex);
    }
    
    public void UpdatePackageColor(int selectedIndex)
    {
        _packageColor.UpdateColor(selectedIndex);
    }
    
    public void UpdateAmmoText(int colorIndex, int ammo)
    {
        _ammo.UpdateAmmoText(colorIndex, ammo);
    }
    
    public void UpdateTimersUI(float timeUntilCanRequestPackage, float timeUntilPackageArrives)
    {
        _timers.UpdateTimersUI(timeUntilCanRequestPackage, timeUntilPackageArrives);
    }
    
    public void UpdateWaveNumber(int waveNumber)
    {
        _currentWave.UpdateWaveNumber(waveNumber);
    }
    
    public void UpdateZombieCount(int zombieCount)
    {
        _currentWave.UpdateZombieCount(zombieCount);
    }
}
