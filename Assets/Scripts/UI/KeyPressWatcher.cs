using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeyPressWatcher : MonoBehaviour
{
    [SerializeField] private int sceneIndex = 1;
    void Update()
    {
        if (Input.anyKey)
        {
            SceneManager.LoadScene(sceneIndex);
        }
    }
}
