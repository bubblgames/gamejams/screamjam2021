using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimersUIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timeUntilCanRequestPackageText;
    [SerializeField] private TextMeshProUGUI timeUntilPackageArrivesText;
    
    public void UpdateTimersUI(float timeUntilCanRequestPackage, float timeUntilPackageArrives)
    {
        timeUntilCanRequestPackageText.text = timeUntilCanRequestPackage <= 0
            ? "Press 'C'!"
            : timeUntilCanRequestPackage.ToString("F");
        timeUntilPackageArrivesText.text = timeUntilPackageArrives <= 0
            ? ""
            : timeUntilPackageArrives.ToString("F");
    }
}
