using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BossHealthUIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _hungerText;
    public void UpdateHealth(int health)
    {
        _hungerText.text = "Hunger: " + health;
    }
}
