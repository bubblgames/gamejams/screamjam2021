using UnityEngine;
using UnityEngine.SceneManagement;

public class BossManager : MonoBehaviour
{
    [SerializeField] private BossUIManager _uiManager;
    [SerializeField] private GameObject _bossPrefab;
    [SerializeField] private PlayerUIManager _playerUIManager;
    
    public void Start()
    {
        StartCoroutine(_uiManager.ShowIntro());
    }

    public void IntroUIFinished()
    {
        _playerUIManager.isPlaying = true;
        SpawnBoss();
    }

    public void SpawnBoss()
    {
        Instantiate(_bossPrefab, _bossPrefab.transform.position, _bossPrefab.transform.rotation);
    }

    public void BossDied()
    {
        _playerUIManager.isPlaying = false;
        StartCoroutine(_uiManager.ShowOutro());
    }

    public void EndUIFinished()
    { 
        SceneManager.LoadScene(5);
    }

    public void UpdateHealth(int health)
    {
        _uiManager.UpdateHealth(health);
    }
}
