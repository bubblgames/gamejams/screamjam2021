using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BossUIManager : MonoBehaviour
{
    private BossManager bossManager;
    private BossHealthUIManager _health;
    [SerializeField] private TextMeshProUGUI _introText;
    [SerializeField] private TextMeshProUGUI _outroText;

    void Awake()
    {
        bossManager = GameObject.Find("BossManager").GetComponent<BossManager>();
        _health = GetComponent<BossHealthUIManager>();
    }

    public IEnumerator ShowIntro()
    {
        _introText.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        _introText.gameObject.SetActive(false);
        bossManager.IntroUIFinished();
    }

    public IEnumerator ShowOutro()
    {
        _outroText.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        _outroText.gameObject.SetActive(false);
        bossManager.EndUIFinished();
    }

    public void UpdateHealth(int health)
    {
        _health.UpdateHealth(health);
    }
}
