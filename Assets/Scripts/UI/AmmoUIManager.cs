using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUIManager : MonoBehaviour
{
    [SerializeField] private List<TextMeshProUGUI> ammoCounts;

    public void UpdateAmmoText(int colorIndex, int ammo)
    {
        ammoCounts[colorIndex].text = ammo.ToString();
    }
}
