using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIColorManager : MonoBehaviour
{
    [SerializeField] private List<Image> colors;
    private float unselectedColorTransparency = 0.33f;

    public void Start()
    {
        UpdateColor(0);
    }
    
    public void UpdateColor(int selectedIndex)
    {
        for (var i = 0; i < colors.Count; i++)
        {
            if (colors[i] != null)
            {
                UpdateImageAlpha(colors[i], i == selectedIndex ? 1f : unselectedColorTransparency);
            }
        }
    }

    private void UpdateImageAlpha(Image image, float newAlpha)
    {
        var tempColor = image.color;
        tempColor.a = newAlpha;
        image.color = tempColor;
    }
}
