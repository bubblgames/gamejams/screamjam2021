using UnityEngine;
public static class BoundaryCheck 
{
    public static bool IsOutOfBounds(float position, float boundary)
    {
        return position > boundary || position < -boundary;
    }
}