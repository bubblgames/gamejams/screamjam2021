using UnityEngine;

public class ZombieCollision : MonoBehaviour
{
    private SpawnManager spawnManager;

    void Awake()
    {
        spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Head"))
        {
            if (ShouldDie(other.gameObject.GetComponent<Renderer>().material.color))
            {
                Destroy(gameObject);
                spawnManager.ZombieDied();
            }
            else
            {
                gameObject.GetComponent<Renderer>().material.color =
                    other.gameObject.GetComponent<Renderer>().material.color;
            }
            Destroy(other.gameObject);
        }
    }

    private bool ShouldDie(Color headColor)
    {
        return GetComponent<Renderer>().material.color.Equals(headColor);
    }
}
