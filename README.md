# Mysterious Green Fuzz

## Game Summary
You got forced into a bad job, now you have to sell skins to skinless people before they take yours!

## Core Mechanics:
* R1- move the character
* R1- aim and shoot skins
* R2- call for more skins
* R3- switch skin type
* R4- Power ups

## Gameplay

### Intro
R2 - Player applies for gamedev role and gets excited but has a nightmare about not reading the terms and conditions and in there it says that the role is nothing like the description and they signed up to sell skins. The player hears a knock at the door and encounters the happy skin salesman (you've met with a terrible fate). Who throws the backpack to the player and runs off into the players room. The player turns around to see skinless people everywhere

### Gameplay
~~R1 - Player moves around with WASD, aims with mouse and shoots skins with clicking. skinnies go towards the player and only turn around when they've been given a skin.~~

R2 - Later waves feature different types of skinnies (different speed, different walking pace, different colors / transparency)

R3 - and different skin requirements

R2 - the player has to refill with skins to keep selling

once 5 waves are completed, the player has a chance to return to his room and post a job listing himself. he posts the same cool job listing and gets a reply from some other eager game dev, and gets his address. He leaves to shift the burden on him.

R2 - final level: something harder idk

R2 - once at the house the player sees the look on the newbies face and recognizes his own reaction, throws the backpack and runs.

End

## Music & Sound FX
Anything from bfxr

## Art Style
Simple 2D sprites

## Animations 
hopefully


## Stages

### R1

1. ~~Main character movement~~
2. ~~Main character shooting skins~~
3. ~~Skinnies walking toward the player~~
4. ~~Intro menu~~
5. ~~Game over~~

### R2

1. ~~Add levels~~
3. Add intro text
4. Add Intermission text
5. add final level
6. Add outro text

### R3

1. ~~add colors for heads (all heads have colors), zombies of the same color are happy with one head, or two of any other head~~
2. air drops for heads -> press a button and choose a color or random basket of more heads, which is later dropped on the field 
3. end of a wave, get a special weapon with a cooldown (come up with specials)
 - grenade
 - faster airdrops
 - flashlight
 - divine shield
 - teleport
 - dash
 - rapid fire
 - multiple heads
 - bigger head
 4. better zombie movement
 - make them walk more unpredictably, change speed while walking
 - changing colors
 - spawning more zombies
 - stealing care package
 - laying down sticky trail to slow you down




### R4
1. Add scoring
5. different terrains or more complex maps for hiding, make player movement more fun
6. boss fight
 - duplication where only one is the bad guy
 - projectiles thrown at you
 - 
